#!/usr/bin/python
"""
Author: Ronald de Jongh 930323409080
A pipeline responsible for handling anything from raw to assembled sequence data and finding cry
proteins in the data.

    inputs:     [1] A string specificying the path to the files to be assembled by idba_ud
"""

# imports
from Bio import SeqIO
from numpy import log

import FFpred_wrapper as fp
from Utility_pipeline_v1 import *


# unused imports
# import os


def run_idba(infile, fastq=False, paired_end=False, l=False):
    """ Runs idba on a fasta/fastq file of NGS reads
    Now also supports fastq, and 2 paired_end fastq files

    :param fastq: (False) True/False flag, for fastq format or not
    :param paired_end: (False) True/False flag, for having 2 files or not, turns infile into a list
    :param infile: path(s) to input fasta file
    :type infile: (str/list)
    :param l: (False) True/False flag, for long or short reads.
    """
    # from the readme:
    # bin/fq2fa --paired --filter read.fq read.fa
    if paired_end and type(infile) != list:
        quit('Please make sure if you have paired end files, that you have a list as infile.')
    idba_dir = tools_dir + "idba-1.1.1/bin/"
    if fastq and paired_end:
        prep_cmd = idba_dir + "fq2fa --merge --filter "
        assert os.path.exists(infile[0])
        prep_cmd += " ".join(infile)
        new_in = ".".join(infile[0].split(".")[:-1]) + "_merged.fasta"
        prep_cmd += " " + new_in
        infile = new_in
        run(prep_cmd)
        if not os.path.exists(infile):
            quit("Something went weird:\n" + infile)
    elif fastq and not paired_end:
        new_in = ".".join(infile.split(".")[:-1]) + "_merged.fasta"
        prep_cmd = idba_dir + "fq2fa --paired --filter " + infile + " " + new_in
        run(prep_cmd)
        assert os.path.exists(new_in)
        infile = "./" + new_in

    cmd = idba_dir + "idba_ud "
    if l:
        cmd += "-l " + infile
    else:
        cmd += "-r " + infile
    cmd += " --maxk 124 "
    cmd += " -o " + a_dir
    run(cmd)


def run_genemark(indir, outdir, mode="all", file_=""):
    """ Runs genemark and makes an extra directory in the data dir with some extra files

    Note: Can be run in with all files in a directory or a specific file

    :param indir: Full path to the directory where all the files to run genemark on are located
    :param outdir: Full path to the directory where all the output files will go
    :param mode: ("all") "all"/"specific", specifies whether you want a specific file or all files
    :param file_: ('') if specific is the mode selected, put the path to your file here
    :produces: Genemark extra .mod and .log files
    :produces: ".faa", ".fnn" and ".gff3" files per input file.
    """
    cmd = "perl " + tools_dir + "genemark/genemark_suite_linux_64/gmsn.pl "
    cmd += "--format GFF3 --faa --fnn "  # output options
    cmd += "--prok " + indir
    if mode == "all":
        cmd += "* "
    elif mode == "specific" and file_ != "":
        cmd += file_ + " "
    else:
        print "Parameters wrongly entered!"
        print "mode param = " + mode
        print "file param = " + file_
        quit()
    run(cmd)
    make_dir("GeneMark_extra_files")
    run("mv *.mod {}/GeneMark_extra_files".format(data_dir))
    run("mv gms.log {}/GeneMark_extra_files".format(data_dir))
    if mode == "specific":
        run("mv {0}.faa {0}.fnn {0}.gff3 {1}".format(file_, outdir))


def check_genemark():
    """ Checks the files made by genemark for double > signs in the headers

    :return: a list of files that contain these things
    """
    check_dir = data_dir + 'gene_prediction/'
    filelist = []
    for root, dirs, filenames in os.walk(check_dir):
        for f in filenames:
            inf = open(os.path.join(root, f), 'r')
            for line in inf:
                if line.rfind('>') > 1:
                    filelist.append(os.path.join(root, f))
                    break
            inf.close()
    return filelist


def regex_bash(filename, pattern):
    """ Takes a filename and a sed pattern, then runs sed on the file specified

    Note: also adds '.fix' before the final extension

    :param filename: the full path to a file
    :param pattern: a string literal of a sed pattern
    """
    cmd = "sed -r '" + pattern + "' "
    cmd += filename + " > "
    if filename[-3:] == "faa":
        cmd += filename[0:-4] + ".fix.faa"
    elif filename[-3:] == "fnn":
        cmd += filename[0:-4] + ".fix.fnn"
    else:
        quit("Extension not recognized!")
    # cmd +=  + re.sub(r'(\..*)(\..*)', r'.fix\2', filename) # such regex, many sads.
    try:
        run(cmd)
    except:
        print pattern
        raise


def try_bt_scanner():  # WIP not sure what to do with his, didn't work when I tested it
    """ Runs the BtToxin_scanner2 (v1.0) program from Ye et al 2012

    Copyright 2008-2016 State Key Laboratory of Agricultural Microbiology, Wuhan, China.
    Email:hstar@webmail.hzau.edu.cn , Elon1133@webmail.hzau.edu.cn

    :produces: ??
    """
    cmd = "cd " + bts_dir + "; "
    cmd += "perl " + tools_dir + "BtToxin_scanner/BtToxin_scanner2.pl "
    cmd += "-s cds "
    cmd += "-p " + data_dir + "gene_prediction/ "
    # cmd += "-4 '.faa' "  # is already the default
    cmd += "-5 '.fnn' "
    run(cmd)


def parse_blast(blastfile, tune=0.5, file_format="qseqid sseqid pident length mismatch" \
                                                 " gapopen qlen slen evalue bitscore"):
    """ Parses a blast file, ensuring high overlap between query and subject and 50% identity

    :param blastfile: The infile, using a fileformat specified as file_format
    :param tune: The maximum allowed difference in size EX: tune=0.5 -> (Q = 100 aa, 50 < S < 150)
    :param file_format: File format used for making the blast file,
    :return: a list of all query ids that matched the set criteria
    """

    with open(blastfile) as inp, open(blastfile + '_highoverlap.txt', 'w') as output:
        output.write("#" + file_format.replace(" ", "\t") + "\n")
        ids = []
        for line in inp.readlines():
            if not line.startswith("#"):
                fields = dict(zip(file_format.split(), line.strip().split('\t')))
                a_len, q_len, s_len = map(float, [fields["length"], fields["qlen"],
                                                  fields["slen"]])
                lower_tune = s_len * tune
                upper_tune = s_len * (tune + 1)
                if float(fields["pident"]) > 50 and (lower_tune < q_len < upper_tune) \
                        and (lower_tune < a_len < upper_tune):
                    if fields["qseqid"] not in ids:
                        ids.append(fields["qseqid"])
                    output.write(line)
        print "Done!\nThis many: {} high overlap hits found".format(len(ids))
    return ids


def hmmscan(infile, outfile, resultfile, db, ali=False, heuristics=False):
    """ Scans a file against an HMM database

    AUTHOR: Guus van de Steeg
    Score cutoff formula: https://compbio.soe.ucsc.edu/ismb99.handouts/KK185FP.html#classify

    :param infile: sequence file to be searched
    :param outfile: direct output of the program.
    :param resultfile: name of the domains found file
    :param db: HMM database file
    :param ali: (False) flag for whether the output should show the alignment
    :param heuristics: (False) flag for whether the heuristics should be dropped.(v Speed, ^ Power)
    :produces: 2 result tables
    """
    cmd = tools_dir + "HMMER3/hmmer-3.0/src/hmmscan "
    if not ali:
        cmd += '--noali '
    if heuristics:
        cmd += '--max '
    N = run("grep '>' {} | wc -l".format(g_dir + "contig.fa.fix.faa"), output=True)
    print "Score-cutoff:", -log(0.01) + log(float(N))
    score_cutoff = -log(0.01) + log(float(N))
    # sigma is 0.01, as low as possible to remove false positives
    T = int(round(score_cutoff + 0.5))  #
    cmd += '-T %d -o %s --tblout %s %s %s' % (T, outfile, resultfile, db, infile)
    run(cmd)


def parse_hmm(domain_table, result_file, overwrite=False, fasta_file=""):
    """ Parses HMM output files looking for the smallest evalue per hit

    :param fasta_file: ("contig.fa.fix.faa") The file to retrieve the protein sequences from
    :param domain_table: Merged Domain file produced by HMMSCAN
    :param result_file: output fasta file of the results, with modified headers for supergroup
                        and evalue (tab separated, so cut will still work)
    :param overwrite: (Default: False) Whether or not to overwrite the result file
    """

    best_dict = {}
    with open(domain_table) as o:
        for line in o:
            if line.startswith("#"):
                continue
            l = line.split()
            sgroup = l[0]
            score = float(l[5])
            qname = l[2]
            if not sgroup[-1].isdigit():  # Assumes its CryAll
                sgroup = "CryAll"
            if qname in best_dict and score > best_dict[qname][1]:
                # if the name is in there, but the score is better, then still take it in
                best_dict[qname] = (sgroup, score)  # {id : (supergroup, score)}
            elif qname not in best_dict:
                best_dict[qname] = (sgroup, score)  # {id : (supergroup, score)}

    if not os.path.exists(result_file) or overwrite:
        with open(result_file, 'w') as a:
            if not fasta_file:
                fasta_file = g_dir + "contig.fa.fix.faa"
            for seq_record in SeqIO.parse(fasta_file, "fasta"):
                if seq_record.id in best_dict.keys():
                    outline = "{}|" * len(best_dict.values()[0])
                    header = seq_record.format("fasta").split()[0] + "\t"
                    header += outline.format(*best_dict[seq_record.id])
                    a.write("\n".join([header, ] + seq_record.format("fasta").split()[1:]) + "\n")
    return os.path.abspath(result_file)


def doHMM():
    hmm_paths = ["".join([hmm_dir, x]) for x in os.listdir(hmm_dir) if x.endswith(
        "HMM")]
    for db in hmm_paths:
        hmmscan(g_dir + "contig.fa.fix.faa", hmm_result_dir + db[-9:-4] + ".HMM_output",
                hmm_result_dir + db[-9:-4] + ".HMM_domains", db, heuristics=True)
    domain_table = hmm_result_dir + basename + ".merged_HMM"
    hmm_result_file = ML_result_dir + basename + ".HMM_results.fasta"
    run("cat " + hmm_result_dir + "*.HMM_domains > " + domain_table)
    result_file = parse_hmm(domain_table, hmm_result_file, overwrite=True)
    return result_file


def organize_output(ML_file, HMM_file, Blast_file):
    """ Generates 3 files with genemark identifiers

    :produces: 3 .list files containing the genemark identifiers found by the three methods
    """
    MLcmd = "grep '>' {} | sed 's/>//' | uniq > RandomForest.list".format(ML_file)
    HMMcmd = "cut -f1 {} | grep '>' | sed 's/>//' | uniq > HiddenMarkovModels.list".format(
        HMM_file)
    Blastcmd = "cut -f1 {} | tail -n+2 | uniq > Blast.list".format(Blast_file)
    run(MLcmd)
    run(HMMcmd)
    run(Blastcmd)


def main():
    # # ASSEMBLY:
    make_dir(a_dir, dat=True)
    # run_idba(filepath, fastq=True, paired_end=True)

    # # GENE PREDICTION:
    make_dir(g_dir, dat=True)
    run_genemark(a_dir, g_dir, mode="specific", file_="contig.fa")
    # genemark sometimes produces fasta files with 2 '>' marks in the header, this causes a mess.
    file_list = check_genemark()
    for fi in file_list:
        if fi.split('.')[-1] == 'faa' or fi.split('.')[-1] == 'fnn':
            regex_bash(fi, r"s/(>.*)(>.*)/\1/")

    # # BT_TOXIN SCANNER (used no more, its obsolete)
    # make_dir(bts_dir, dat=True)
    # try_bt_scanner()

    # # BLAST
    # make_dir(ML_result_dir, dat=True)
    blast_report = ML_result_dir + basename + ".blast"
    # print "Blast all gene-marked genes against the cry protein database"
    # doBlastP(g_dir + "contig.fa.fix.faa")
    # parse_blast(g_dir + "contig.fa.fix.faa.blast")
    # run("mv " + g_dir + "contig.fa.fix.faa.blast_highoverlap.txt " + blast_report)

    # # HMMSCAN
    print "Do hmmscan against all hmms in 'my_HMM_models/' "
    make_dir(hmm_result_dir, dat=True)
    hmm_result_file = doHMM()

    # # My Machine Learning
    print "Run feature prediction for all cry supergroups"
    fp.test_model(model_dir + "Cry2_vs_web_TrEMBL.pkl", g_dir + "contig.fa.fix.faa", 0)
    ML_file = ML_result_dir + "Model_Output.fasta"
    run("mv " + g_dir + "contig.fa.fix.faa" + ".ML.fasta " + ML_file)

    # # Organize Output
    print "Creating the .list files for further analysis"
    organize_output(ML_file, hmm_result_file, blast_report)

if __name__ == '__main__':
    # Private dirs
    a_dir = data_dir + 'Assembly/'
    hmm_result_dir = data_dir + 'HMM_results/'
    bts_dir = data_dir + 'bts/'
    #
    # # TODO Think about user input, like support for paired-end & fastq

    # # PARSE INPUT
    # try:
    #     filepath = argv
    # except KeyError:
    #     print __doc__
    #     raise

    filepath = ["/scratch/ronald/Lysini_data/NG-10616_IGEM_lib145248_4889_3_1.fastq",
                "/scratch/ronald/Lysini_data/NG-10616_IGEM_lib145248_4889_3_2.fastq"]
    basename = os.path.basename(filepath[0])
    # main()
    parse_blast("/")
