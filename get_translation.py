#!/usr/bin/env python
"""
Author: Ronald de Jongh
"""

# import statements
import Bio.SeqIO

import FFpred_wrapper as fp
from Bioinformatics.Known_Protein import make_multiple_hmms, make_faa
from Utility_pipeline_v1 import *

# Obsolete imports
import os


def get_translation(infile):
    """ Parses all '/translation' features out of genbank/EMBL files

    :param infile: file to parse the translation feature from
    :return: Protein sequence
    """
    cap1, cap2 = False, False
    pd = []
    # dd = []  # support for DNA sequences still WIP
    for line in infile:
        if not line.startswith("FT"):
            cap1 = False
        elif line.split()[1].startswith("/translation"):
            cap1 = True
        if cap1 and not len(line.split()) > 2:
            pd.append(line)
        if line.endswith('\"\n'):
            cap1 = False
            # if line.startswith("SQ"):
            #     cap2 = True
            # if cap2:
            #     dd.append(line)

    if not pd:
        return ""
    try:
        protein = pd[0].split("\"")[1].strip()
    except IndexError:
        quit(infile.name)

    for seq in pd[1:]:
        protein += seq.split()[1]
        if seq.endswith("\""):
            print seq
            break
    # if protein:
    return protein[:-1]


def write_allCry2():
    #
    # WIP some sequences don't have the built in /translation thingy used by get
    # translation, so instead we reuse the old pipeline a bit.
    #
    alt_list = []
    with open(cry_prot_dir + "AllCry2.fasta", 'w') as o:
        for gb in file_gen("/scratch/ronald/data/cry_GB_test/", "gb"):
            with open(gb) as i:
                prot = get_translation(i)
                if prot:
                    o.write(">{}\n{}\n".format(os.path.basename(gb).split(".")[0], prot))
                else:
                    alt_list.append(gb)
    if alt_list:
        make_faa(alt_list, cry_prot_dir)
        group_cry(cry_prot_dir)


def group_cry(indir, overwrite=True):
    """ Concatenates a bunch of .fasta files in a directory into super files

    Based on numbers in the 4th and 5th place of the filename

    :param indir: directory of fasta files
    :param overwrite: (Default True) whether or not to remove all group_fasta files before
    starting, since the function always appends this will add to previous iterations
    """
    # nuclear option:
    if overwrite and list(file_gen(indir, "group_fasta")):
        run("rm {}*.group_fasta".format(indir))

    for f in file_gen(indir, ".gb"):
        identifier = create_name(os.path.basename(f))
        group_name = indir + identifier + ".group_fasta"
        with open(group_name, 'a') as group_file:
            group_file.write(">{}\n{}\n".format(os.path.basename(f), get_translation(open(f))))


def create_name(filename):
    """ Takes a filename and returns the group file name

    :param filename: the name of a file to be grouped
    :type filename: str
    :return: a 5 letter identifier for a group name
    """
    cry_cyt = filename[0:3]
    if filename[4].isdigit():  # assumes less than 100 cry groups, current max is 73
        i = filename[3] + filename[4]
    else:
        i = "0" + filename[3]
    assert len(i) == 2  # seriously.
    return cry_cyt + i


def align_files(indir, outdir, q=False):
    """ Aligns proteins in .faa files to each other, creating .sto files using clustalo

    Note: does ALL .faa files in that directory

    :param indir: directory where the .faa files are
    :param outdir: directory to put them
    :param q: flag for quiet mode, will output the output of clustalo to a file in the data_dir
    :type q: Bool
    :prints: files that were not processed
    """

    for f in file_gen(indir, ".group_fasta"):
        out = f.split('.')[0] + ".sto"
        cmd = "clustalo"
        cmd += " -i " + f
        cmd += " -o " + out
        cmd += " --outfmt=st -v "
        if q:
            cmd += "> " + data_dir + "clustalo_output.txt; "
        # in this instance we need to continue if the check is messed up because
        # there might be single file proteins
        exitcode = subprocess.call(cmd, shell=True)

        # but we still wanna know when it happens
        if exitcode != 0:
            print "This file was not processed: " + f

    mvcmd = "mv " + indir + "*.sto " + outdir
    run(mvcmd)


if __name__ == "__main__":
    cry_prot_dir = data_dir + "group_faa/"
    group_align = data_dir + "align_sto/"
    model_dir = data_dir + "model_csv_dir"
    write_allCry2()
    # group_cry("/scratch/ronald/data/cry_GB_test/")
    # run("mv {}*.group_fasta {}".format("/scratch/ronald/data/cry_GB_test/", cry_prot_dir))

    blastdbcmd = "makeblastdb -in " + cry_prot_dir + "AllCry2.fasta -dbtype 'prot' -out " \
                                                     "./cry_blast_db/Cry_Prot_db"
    run(blastdbcmd)

    make_dir(group_align, dat=True)
    print "Make a multiple sequence alignment of the .group_fasta files"
    align_files(cry_prot_dir, group_align)

    make_dir(hmm_dir, dat=True)
    print "Make multiple hmms for all cry supergroups"
    make_multiple_hmms(group_align, hmm_dir)

    make_dir(model_dir, dat=True)
    print "Create the model separating cry from cry-lookalikes"
    target_infile = cry_prot_dir + "CryALL.faa"
    ran_infile2 = "/home/ronald/Trembl_not_cry.fasta"
    targ = Bio.SeqIO.index(target_infile, 'fasta').values()
    target_infile2 = "/home/ronald/new_target.faa"
    if not os.path.exists(target_infile2):
        with open(target_infile2, 'w') as o:
            itr = Bio.SeqIO.index(ran_infile2, 'fasta').keys()
            for noncry in range(len([i for i in itr])):
                rec = targ.next()
                o.write(">" + rec.id + "\n" + str(rec.seq)[:-1] + "\n")

    fp.main(target_infile2, ran_infile2,
            model_file_name="Cry2_vs_web_TrEMBL.pkl", plot_bounds=True)
    fp.analyse_RF(model_file_name="Cry2_vs_web_TrEMBL.pkl")
