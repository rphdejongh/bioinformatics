This set of scripts is meant to show the exact process of how the bioinformatics
part of my thesis went, and essentially being able to rerun everything at the 
push of a button.

FFpred_wrapper: Code for doing various kinds of model reading and creation
    through the use of sci-kit learn.

known_protein_pipeline_v1: Script to download, sort, organize, clean the cry and
    cyt genes from a 2 column csv file. Will also translate them into proteins 
    and then create a blast database (nuc and prot) MSAs (prot) and HMMs (prot) 
    of all of them.

sequence_data_pipeline_v1: Script responsible for handling anything from 
    raw to assembled sequence data and finding cry proteins in the data. 
    By the use of idba_ud, genemark, bt_toxin_scanner, blast, 
    hmmscan (HMMER v3.0) and my own models.

Utility_pipeline_v1: Script to contain any and all functions that are shared by 
    for the rest of my Thesis pipelines.