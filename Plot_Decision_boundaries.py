""" A series of functions to create a figure for decision boundaries from a RandomForest classifier

AUTHOR: Cameron Mence
EDITS: Ronald de Jongh
BLOG URL: http://www.subsubroutine.com/sub-subroutine/2016/2/15/understanding-machine-learning \
-techniques-by-the-decision-boundaries-they-are-capable-of
"""
import itertools
import numpy as np

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import cm
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier


def plotcases(ax, pair, xdf, Y):
    """ Function to plot observations on scatterplot """
    plt.scatter(xdf[pair[0]], xdf[pair[1]], c=Y, cmap=cm.coolwarm, axes=ax, alpha=0.6, s=20,
                lw=0.4)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.tick_params(axis='both', which='both', top='off', right='off', bottom='off', left='off',
                   labelleft='off', labelright='off', labeltop='off', labelbottom='off')


def plotboundary(ax, Z, xx, yy):
    """ Function to draw decision boundary and colour the regions """
    ax.pcolormesh(xx, yy, Z, cmap=cm.coolwarm, alpha=0.1)
    ax.contour(xx, yy, Z, [0.5], linewidths=0.75, colors='k')


def standardize_dataframe(X, column_names=()):
    """ Takes a data_array and a class_array and turns it into a standardized pandas dataframe """
    mean = X.mean(axis=0)
    std = X.std(axis=0)
    X = (X - mean) / std

    if type(column_names) == str:
        column_names = column_names.split()

    if len(column_names) != X.shape[1]:
        print "Column names argument failed, using standard xNumber notation"
        column_names = ['x' + str(x + 1) for x in range(X.shape[1])]

    return pd.DataFrame(X, columns=column_names)


def create_fig(rf, xdf, Y, title):
    """  Creates the decision boundary figure from a model, dataframe and class array

    :param rf: the Random forest model
    :param xdf: standardized pandas dataframe with ONLY DATA (no classes)
    :param Y: class list/array
    :param title: Filename of the plot and title given in the plot
    :produces: A png file with "title" as filename
    """

    pairs = list(itertools.combinations(xdf.columns, 2))

    nrows = ncols = int(round((len(pairs) ** .5) + .5))
    if nrows * ncols - len(pairs) > nrows:  # if the diff is greater than a whole row, remove it
        # what if the difference is more than 1 row?
        nrows -= int(round((nrows * ncols - len(pairs)) / nrows))  # seems excessive though
    gridsize = (nrows, ncols)

    plt.figure(dpi=400 * ncols)
    plt.title(title)
    plt.subplots_adjust(hspace=.5)

    x_min = y_min = round(min(list(xdf.min())) + .5)
    x_max = y_max = round(max(list(xdf.max())) + .5)
    nx, ny = 100, 100  # this sets the num of points in the mesh
    xx, yy = np.meshgrid(np.linspace(x_min, x_max, nx),
                         np.linspace(y_min, y_max, ny))

    coords = [(x, y) for x in range(nrows) for y in range(ncols)]

    for i, pair in enumerate(pairs):
        x_coord = coords[i][0]
        y_coord = coords[i][1]
        rf.fit(xdf[[pair[0], pair[1]]], Y)

        Z = rf.predict_proba(np.c_[xx.ravel(), yy.ravel()])
        Z = Z[:, 1].reshape(xx.shape)

        ax = plt.subplot2grid(gridsize, [x_coord, y_coord])
        ax.title.set_text(str(pair))
        ax.title.set_size('small')
        plotboundary(ax, Z, xx, yy)
        plotcases(ax, pair, xdf, Y)

    plt.savefig(title)


if __name__ == "__main__":
    rf = RandomForestClassifier()
    X, y = make_classification(n_samples=100, n_features=6, n_informative=2, n_redundant=0,
                               n_repeated=0, n_classes=2, n_clusters_per_class=2, weights=None,
                               flip_y=0.02, class_sep=0.5,
                               hypercube=True, shift=0.0, scale=0.5, shuffle=True, random_state=5)
    xdf = standardize_dataframe(X, y)
    create_fig(rf, xdf, y, "Example_decision_boundaries")
