#!/usr/bin/env python
"""
This pipeline will retrieve all files given a comma delimited list of names,EBI accession numbers

Author: Ronald de Jongh

                           -> MSA -> HMM  v
known nucl cds files -> aa -> RF/...  -> .list files of hits
                           -> Blastdb     ^
"""

# import statements
from __future__ import division

import Bio.SeqIO
from Bio.Alphabet import IUPAC
from Bio.Data.CodonTable import TranslationError
from Bio.Seq import Seq

import FFpred_wrapper as fp
from Utility_pipeline_v1 import *


# unused import statements
# import Bio
# import os
# import subprocess
# import sys


def protein_convert(dna):
    """This function will try its best to translate a DNA sequence from a string

    Note: Has support for non Bio string sequences)

    :param dna: String of DNA or biopython seq object
    :return: a biopython seq object with the translated sequence.
    """

    if type(dna) == Seq:
        return dna.translate()
    elif type(dna) == str:
        try:
            return Seq(dna, IUPAC.ambiguous_dna).translate()
        except TranslationError:
            print "This DNA is messed up:\n" + dna + "\n"
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
    else:
        raise TypeError("Expected a string or a Bio.Seq.Seq object")


def make_faa(file_list, outdir, rubbish_directory, dl_dict="", inflag=True):
    """ Function to turn a list of grouped fasta files into grouped faa files

    Note: bad proteins will have their fasta files removed, with a descriptive prefix

    :param file_list: list of filepaths to the fasta files
    :param outdir: Directory where the files should be moved
    :param rubbish_directory: directory to which bad files should be moved
    :param dl_dict: dictionary with the results of the download (generated from getCry.sh)
    :type dl_dict: dict {name : [url, header, file_linelength, relative_filepath]}
    :param inflag: (Default: True), check whether or not user input should be used
    :type inflag: Bool
    :return stoplist: a list of fasta filenames that have been moved
    """
    stoplist = []
    for file_path in file_list:
        outname = os.path.basename(file_path).split('.')[0] + ".faa"
        with open(outdir + outname, 'w') as outfile:
            for seq_record in Bio.SeqIO.parse(file_path, "fasta"):
                header = ">" + seq_record.description
                try:
                    seq_record.seq = seq_record.seq.translate()
                    # translates the seq object internally

                except TranslationError:
                    print "This file can not be translated:"
                    print header + "\n" + seq_record.seq
                except:
                    print "Unexpected error:", sys.exc_info()[0]
                    raise

                if seq_record.seq.count("*") > 1 or \
                        (seq_record.seq.find("*") != len(seq_record.seq) - 1 and
                                 seq_record.seq[-1] != "*"):
                    print "This file has many stop codons:"  # + "\n" + seq_record.seq
                    print header + "\n" + seq_record.seq
                    # removal_subroutine(file_path, rubbish_directory, "many_stop_codons", inflag)

                outfile.write(seq_record.format('fasta'))


def read_download_results(dlf, spacer_="\\t"):
    """ Reads in the downloadfile and produces a dictionary

    :param dlf: path to the download file
    :param spacer_: the spacer_ that was used in the creation of the download file
    :return: dict: {name : [url, header, relative_filepath, file_linelength]}
    """
    d_dict = {}
    with open(dlf) as dlf:
        for line in dlf:
            line_list = line.strip().split(spacer_)
            d_dict[line_list[0]] = line_list[1:-1] + line_list[-1].split()
    return d_dict


def translate_group_files(indir, outdir, rubbish_directory, file_dict="", check=True):
    """ This will run through a directory with .group_fasta files pass a list to make_faa

    Note: And then move them to the outdir

    :param indir: the directory where the input files are located
    :param outdir: directory to put the group.faa files
    :param rubbish_directory:
    :param file_dict: A dictionary of the download results
    :type file_dict: dict: {name : [url, header, relative_filepath, file_linelength]}
    :param check: True False check for whether existing files should be appended to
    :prints: The files that were caught will be printed in a a regular list format
    """
    file_list = []
    for f in file_gen(indir, ".group_fasta"):
        file_list.append(f)

    stop_list = make_faa(file_list, outdir, rubbish_directory, file_dict, inflag=False)
    print "These files might be messed up: "
    print stop_list


def align_files(indir, outdir, q=False):
    """ Aligns proteins in .faa files to each other, creating .sto files using clustalo

    Note: does ALL .faa files in that directory

    :param indir: directory where the .faa files are
    :param outdir: directory to put them
    :param q: flag for quiet mode, will output the output of clustalo to a file in the data_dir
    :type q: Bool
    :prints: files that were not processed
    """

    for f in file_gen(indir, ".faa"):
        out = f.split('.')[0] + ".sto"
        cmd = "clustalo"
        cmd += " -i " + f
        cmd += " -o " + out
        cmd += " --outfmt=st -v "
        if q:
            cmd += "> " + data_dir + "clustalo_output.txt; "
        # in this instance we need to continue if the check is messed up because
        # there might be single file proteins
        exitcode = subprocess.call(cmd, shell=True)

        # but we still wanna know when it happens
        if exitcode != 0:
            print "This file was not processed: " + f

    mvcmd = "mv " + indir + "*.sto " + outdir
    run(mvcmd)


def make_hmm(model_name, model_dir, seq_align_file):
    """Makes an hmm out of a multiple sequence alignment (just the bash-call)

    :param model_name: name of the model (output)
    :param model_dir: directory where to put the model (output)
    :param seq_align_file: the full path to the sequence file
    :produces: hmmpressed hidden markov model of a stockholm alignment file in a special directory
    """
    hmmer3_dir = tools_dir + "HMMER3/hmmer-3.0/src/"
    cmd = "cd " + model_dir + "; "  # go to model dir
    cmd += hmmer3_dir + "hmmbuild "  # call the hmmbuild function
    cmd += model_name + " "  # name the model
    cmd += seq_align_file + "; "  # give the path to the sequence file
    cmd += hmmer3_dir + "hmmpress " + model_dir + model_name + "; "  # press the model
    run(cmd)


def make_multiple_hmms(indir, hmm_directory):
    """ Loops through a directory and creates hmms from all alignment files it finds

    :param indir: alignment directory
    :param hmm_directory: directory to put the hmms
    """
    for f in file_gen(indir, ".sto"):
        identifier = os.path.basename(f)[:-4]
        make_hmm(identifier + "_HMM", hmm_directory, f)


def build_database(input_fasta, db_type, database_name, out_path):
    # type: (string, string, string, string) -> Bool
    """ Method to build a blast database using makeblastdb.

    :param input_fasta: The path of the fasta file to be used, given as a string.
    :param db_type: The type of database, given as a string.
    :param database_name: The name of the database, given as a string.
    :param out_path: The path to the output folder, given as a string.
    :return exit_code: True/False depending on whether the database was built or not.
    :prints: status
    """
    path_tests = ['', '', '']
    for index, extension in enumerate(['nhr', 'nin', 'nsq']):
        path_tests[index] = '%s/%s.%s' % (out_path, database_name, extension)
    if not all(os.path.exists(file_path) for file_path in path_tests):
        print('Database being built.')
        cmd = 'makeblastdb -in %s -dbtype %s -out %s' % (input_fasta, db_type, database_name)
        run(cmd)
        mv_cmd = 'mv ' + database_name[:-1] + '* ' + out_path
        run(mv_cmd)
        return True
    else:
        print('Database already built.')
        return False


def group_cry(indir, overwrite=False):
    # WIP todo this function will always murder all '*.group_fasta files'
    """ Concatenates a bunch of .fasta files in a directory into super files

    Based on numbers in the 4th and 5th place of the filename

    :param indir: directory of fasta files
    :param overwrite: (Default False) whether or not to remove all group_fasta files before
    starting
    :prints: skipped files
    """
    f = "nothing"
    # nuclear option:
    if overwrite:
        run("rm {}*.group_fasta".format(indir))

    for f in file_gen(indir, ".fasta"):
        identifier = create_name(os.path.basename(f))
        group_name = indir + identifier + ".group_fasta"
        with open(group_name, 'a') as group_file:
            group_file.write(open(os.path.join(indir, f), 'r').read() + "\n")

    else:
        print "Skipped: " + f


def create_name(filename):
    """ Takes a filename and returns the group file name

    :param filename: the name of a file to be grouped
    :type filename: str
    :return: a 5 letter identifier for a group name
    """
    cry_cyt = filename[0:3]
    if filename[4].isdigit():  # assumes less than 100 cry groups, current max is 73
        i = filename[3] + filename[4]
    else:
        i = "0" + filename[3]
    assert len(i) == 2  # seriously.
    return cry_cyt + i


def run_get_cry(enddir, dlf="", inflag=True):  # WIP ASSUMES DOWNLOAD ALREADY HAPPENED
    """ Runs the get cry bash script with a list of known cry proteins

    :param inflag: (Default: True), flag for checking whether or not user input is to be used.
    :param dlf: (Default: ""), the full path of the download file to write out
    :param enddir: directory where the files will be stored
    :return spacer_: the spacer used for the download_results.csv file
    :prints Prompt1: For spacer evaluation
    :prints Prompt2: For overwriting the download file
    :prints: status of download file
    """
    # spacer_ = "\t"
    cmd = "bash " + script_dir + "getCry.sh "
    cmd += data_dir + "trimmed_ebi_list.csv "
    cmd += enddir + " "
    # cmd += spacer_
    if dlf != "" and not os.path.exists(dlf):
        cmd += " > " + dlf
    elif os.path.exists(dlf):
        if inflag:
            i = raw_input("Download already exists! Overwrite? \n(Y/N): ")
            if i.upper() == "Y" or i.upper() == "YES":
                cmd += " > " + dlf
        else:
            cmd += " > " + dlf
    else:
        print "Not making a download file"
    run(cmd)
    # return spacer_


def move_rubbish_cry(outdir, dl_dict, inflag=True):
    """ This function does cleaning up of the retrieved cry files

    (using common flags in the header)
    except for too long files, they will be moved when checked by the translation

    :param dl_dict: dictionary with the results of the download (generated from getCry.sh)
    :type dl_dict: dict {name : [url, header, file_linelength, relative_filepath]}
    :param outdir: directory to put the rubbish files (with name change)
    :param inflag: True False whether or not user input should be used
    """
    for line_list in dl_dict.itervalues():
        url = line_list[0]
        header = line_list[1]
        file_linelength = line_list[2]
        relative_filepath = line_list[3]
        # if len(line_list) > 4:
        #     quit("\n".join(line_list))
        if header == "":
            removal_subroutine(relative_filepath, outdir, "no_header", inflag)
        if not url.startswith("http"):
            removal_subroutine(relative_filepath, outdir, "no_link", inflag)
        if header.count(">") > 1:
            removal_subroutine(relative_filepath, outdir, "multi_header", inflag)
        # specifier_list = ['partial', 'fragment', 'plasmid', 'not found', 'suppressed']
        # for x in specifier_list:
        #     if header.find(x) > 0 and header.find("complete cds") == -1:
        #         removal_subroutine(relative_filepath, outdir, x.replace(" ", "_"), inflag)
        #    elif header.find("complete") != -1:
        #        removal_subroutine(relative_filepath, outdir, x.replace(" ", "_"), True)
        #    else:
        #        continue

        if file_linelength < 5:
            removal_subroutine(relative_filepath, outdir, "too_short", inflag)


def move_rubbish_fasta2(indir, outdir, inflag=True):
    """ This function does cleaning up of the retrieved cry files

    except for too long files, they will be moved when checked by the translation
    This Function assumes short files which can be put in memory whole.

    :param indir: directory of the fasta sequences.
    :param outdir: directory to put the rubbish files (with name change)
    :param inflag: True False whether or not user input should be used
    """
    bad_files = []
    for fasta_file in file_gen(indir, "fasta"):
        lines = open(fasta_file).read().split("\n")

        if len(lines) <= 5:
            removal_subroutine(fasta_file, outdir, "too_short", inflag)
            bad_files.append(fasta_file)
            continue
        if not lines[0].startswith(">"):
            removal_subroutine(fasta_file, outdir, "no_header", inflag)
            bad_files.append(fasta_file)
            continue
        if sum([l.startswith(">") for l in lines]) != 1:
            removal_subroutine(fasta_file, outdir, "multi_header", inflag)
            bad_files.append(fasta_file)
            continue


def removal_subroutine(filepath, outdir, specifier, inflag=True):
    """ Moves a file based on user input,

    :param filepath: Full path to the file to be removed
    :param outdir: directory to which the file should be moved
    :param specifier: reason why the file was moved, will end up in the filename
    :param inflag: True False whether or not user input should be used
    :return: Bool whether or not the file actually existed
    """
    if os.path.exists(filepath):
        if inflag:
            print "Bad file: " + filepath
            print "Reason: " + specifier
            i = raw_input("Move file to rubbish? \n(Y/N):")
            if i.upper() == "Y" or i.upper() == "YES":
                run("mv " + filepath + " " + outdir + '.' + specifier + filepath.split(r'/')[-1])
        else:
            run("mv " + filepath + " " + outdir + specifier + "_" + filepath.split(r'/')[-1])
        return True
    else:
        return False


def concat_fasta(indir):
    """ Concatenates a bunch of fasta files in a directory """
    if not os.path.exists(indir + "all_cry.fa"):
        # the big file should explicitly have a different extension in case the above fails.
        run("cat " + indir + "*.fasta > " + indir + "all_cry.fa")
    else:
        print "all_cry.fa exists already!\n"


def retrieve_lookalikes(outpath, uniprot_db="/scratch/databases/blastdb/UniProt/TrEMBL/"
                                            "UniProt_TrEMBL_11-May-2016"):
    """ Retrieves lookalike proteins from the uniprot database
    :param uniprot_db: database to retrieve lookalikes from
    :param outpath: path to the outfile (including a logical name)
    """

    if not os.path.exists(cry_prot_dir + "CryALL.faa"):
        run("cat {0}*.faa > {0}CryALL.faa".format(cry_prot_dir))
    file_format = "qseqid sseqid pident length mismatch gapopen qlen alen evalue bitscore sstrand"
    if not os.path.exists(cry_prot_dir + "CryALL.faa.blast"):
        doBlastP(cry_prot_dir + "CryALL.faa", db=uniprot_db, evalue=20, threads=16,
                 file_format=file_format, max1=False)
    # ref_bitscore = bitscore_real_hit / 2  # Would be the prefered method
    hit_dict = {}  # {subject id : [query id, bitscore, subject sequence]}
    #  I want to save all subjects
    with open(outpath, 'w') as o:
        for line in open(cry_prot_dir + "CryALL.faa.blast"):
            linedict = dict(zip(file_format.split(" "), line.split()))
            # WIP In TremBL I used the Domains/annotation/GO terms to filter out cry proteins, here
            # I don't know

            if linedict['pident'] < 95 or (
                            linedict['qlen'] == linedict['alen'] and linedict['pident'] < 80):
                # or linedict['bitscore'] < ref_bitscore :
                hit_dict[linedict['sseqid']] = [linedict['qseqid'], linedict['bitscore'],
                                                linedict['sstrand']]  # sstrand doesn't always work
    for hit in hit_dict:
        o.write(">{}\t{}\n{}\n\n".format(*hit_dict[hit]))


if __name__ == "__main__":
    # POTENTIAL INPUTS:
    # data_dir, (cry_cyt_lists2.csv OR trimmed_ebi_list.csv)

    # make_dir(data_dir, dat=True)

    # private dirs
    cry_dna_dir = data_dir + "cry_prots_lifesci/"
    rubbish_dir = data_dir + "rubbish_dir/"
    cry_prot_dir = data_dir + "group_faa/"
    group_align = data_dir + "align_sto/"
    model_dir = data_dir + "model_csv_dir"

    download_file = data_dir + "download_results.csv"

    make_dir(cry_dna_dir, dat=True)
    print "Retrieve cry & cyt proteins from a csv list"
    run_get_cry(cry_dna_dir, download_file)
    download_dict = read_download_results(download_file, "\\t")

    make_dir(rubbish_dir, dat=True)
    print "Check for crappy cry proteins and move them"
    move_rubbish_fasta2(cry_dna_dir, rubbish_dir, inflag=False)

    make_dir(blast_dir, dat=True)
    print "Make a single big file for all cry proteins, to blast against"
    concat_fasta(cry_dna_dir)
    build_database(cry_dna_dir + "all_cry.fa", "nucl", "Cry_nucl_db", blast_dir)

    print "Group the known fasta files by supergroup as per the official naming convention"
    print "WITH ZEROES in the 10s place for cry groups below 10 to avoid confusion and weirdness"
    group_cry(cry_dna_dir, overwrite=True)
    # replaces: run("bash " + script_dir + "group_cry.sh")

    make_dir(cry_prot_dir, dat=True)
    print "Translate the grouped DNA fasta files to .faa files"
    translate_group_files(cry_dna_dir, cry_prot_dir, rubbish_dir, check=False)

    make_dir(group_align, dat=True)
    print "Make a multiple sequence alignment of the .faa files"
    align_files(cry_prot_dir, group_align)

    make_dir(hmm_dir, dat=True)
    print "Make multiple hmms for all cry supergroups"
    make_multiple_hmms(group_align, hmm_dir)

    make_dir(model_dir, dat=True)
    print "Create the model separating cry from cry-lookalikes"
    target_infile = cry_prot_dir + "CryALL.faa"
    # ran_infile = data_dir + "TrEMBL_cry_lookalikes.fasta"
    # if not os.path.exists(ran_infile):
    #     retrieve_lookalikes(ran_infile)
    # WIP still want to get only those cry proteins that resemble the lookalikes

    # fp.main(target_infile, ran_infile, model_file_name="Cry_vs_local_TrEMBL", plot_bounds=True)

    ran_infile2 = "/home/ronald/Trembl_not_cry.fasta"
    targ = Bio.SeqIO.index(target_infile, 'fasta').values()
    target_infile2 = "/home/ronald/new_target.faa"
    if not os.path.exists(target_infile2):
        with open(target_infile2, 'w') as o:
            itr = Bio.SeqIO.index(ran_infile2, 'fasta').keys()
            for noncry in range(len([i for i in itr])):
                rec = targ.next()
                o.write(">" + rec.id + "\n" + str(rec.seq)[:-1] + "\n")

    # fp.main(target_infile2, ran_infile2,
    #         model_file_name="Cry2_vs_web_TrEMBL.pkl", plot_bounds=True)
    fp.analyse_RF(model_file_name="Cry2_vs_web_TrEMBL.pkl")

    print os.getcwd()
