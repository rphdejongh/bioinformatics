#!/usr/bin/python

"""
Author: Ronald de Jongh 930323409080
A repository for code that was written to test certain algorithms, but did not make it to the
final pipeline.

    inputs:     [1] ?
"""
import Bio
import Bio.SeqIO
import itertools
import numpy as np
import os
import pickle
from Bio import SearchIO
from Bio.Data.IUPACData import protein_letters
from Bio.SeqUtils import ProtParamData
from Bio.SeqUtils.ProtParam import ProteinAnalysis

from matplotlib import pyplot as plt
from matplotlib.mlab import PCA
from sklearn import svm, preprocessing
from sklearn.cluster import estimate_bandwidth, MeanShift
from sklearn.cross_validation import cross_val_score, permutation_test_score, StratifiedKFold, \
    train_test_split
from sklearn.decomposition import KernelPCA
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import roc_curve, auc, confusion_matrix

from Bioinformatics.FFpred_wrapper import read_single_csv, chunk, test_model, bin_size
from Utility_pipeline_v1 import ff_tools_dir, run, \
    model_dir, data_dir, sequence_clean


# import thread


class SvmThings:
    def __init__(self):
        """
        ideas:
        estimator = some_func()
        true data, true labels = np.array([]), np.array([])

        confusion_matrix = confusion_matrix(y_test, pred)
        """
        return

    @staticmethod
    def check_grid_svm(param_grid, dat, cla, outfile):
        grid_svm = GridSearchCV(svm.SVC(), param_grid, cv=10, verbose=3, n_jobs=8)
        grid_svm.fit(dat, cla)
        if not os.path.exists(outfile):
            with open(outfile, "wb") as svm_outputfile:
                pickle.dump(grid_svm.best_estimator_, svm_outputfile)
                with open("ML_attempts.txt", "a") as ml_meta:
                    ml_meta.write(str(grid_svm.grid_scores_))
        else:
            return

    def Threaded_SVM_grid_search(data_array, class_array, ):
        param_grid1 = {'C': [0.1, 1, 10], 'kernel': ['linear']}
        param_grid2 = {'C': [0.1, 1, 10], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}
        try:
            thread.start_new_thread(self.check_grid_svm, (param_grid1, data_array, class_array,
                                                          "linear_svms.pkl"))
            thread.start_new_thread(self.check_grid_svm, (param_grid2, data_array, class_array,
                                                          "rbf_svms.pkl"))
        except Exception:
            print "Error: unable to start threads"

    @staticmethod
    def linear_svm(X_train, X_test, y_train, y_test):
        SVM40 = svm.LinearSVC().fit(X_train, y_train)
        norm_score = SVM40.score(X_test, y_test)
        return norm_score

    @staticmethod
    def Scaled_Linear_SVM(X_train, X_test, y_train, y_test):
        scaler = preprocessing.StandardScaler().fit(X_train)
        X_train_transformed = scaler.transform(X_train)
        scaleSVM40 = svm.LinearSVC().fit(X_train_transformed, y_train)
        X_test_transformed = scaler.transform(X_test)
        scaleSVM40.score(X_test_transformed, y_test)
        scale_score = scaleSVM40.score(X_test, y_test)
        return scale_score

    @staticmethod
    def svm_internal_cv(data_array, class_array):
        clf = svm.LinearSVC(dual=False)
        scores = cross_val_score(clf, data_array, class_array, cv=5, n_jobs=5)
        print clf.get_params()
        return scores

    @staticmethod
    def sigmoid_svm(data_array, class_array):
        clf = svm.SVC(kernel="sigmoid", verbose=2, C=100, gamma=0.001)
        scores = cross_val_score(clf, data_array, class_array, cv=5, n_jobs=5)
        print clf.get_params()
        return scores


def visualize_pca(pca, header, plotname="Cry_data_biplot"):
    sorted_PC1 = sorted(zip(header[1:], list(pca.Wt[:, 0])), key=lambda t: t[1])
    sorted_PC2 = sorted(zip(header[1:], list(pca.Wt[:, 1])), key=lambda t: t[1])
    print "Top 5 contributing values of PC1:"
    for x in sorted_PC1[:5]:
        print "{: <16}: {: >7.2%}".format(*x)
    print "Top 5 contributing values of PC2:"
    for x in sorted_PC2[:5]:
        print "{: <16}: {: >7.2%}".format(*x)
    # # create biplot(s)
    biplot(pca.Y[:, 0:2], pca.Wt[:, 0:2], header[1:]).savefig(plotname)
    if pca.fracs[3] > 0.1:
        biplot(pca.Y[:, 1:3], pca.Wt[:, 1:3]).savefig("Cry_data_biplot_2v3")
        biplot(pca.Y[:, 0] + pca.Y[:, 3], pca.Wt[:, 0] + pca.Wt[:, 3]).savefig(
            "Cry_data_biplot_1v3")


def do_permutated_rf(cv_obj, data_array, class_array):
    """ Shows off the significance of the cross validated score
    histogram code url: http://scikit-learn.org/stable/auto_examples/feature_selection
    /plot_permutation_test_for_classification.html

    :param class_array:
    :param data_array:
    :param cv_obj:
    :return:
    """
    clf = RandomForestClassifier(warm_start=True, oob_score=True, n_estimators=1500, n_jobs=8)
    score, perm_scores, pvalue = permutation_test_score(clf, data_array, class_array, cv=cv_obj,
                                                        n_jobs=8, verbose=2)
    print "Score without permutation:", score
    print "Average permutation score:", np.mean(perm_scores), "out of", len(perm_scores), "scores"
    plt.hist(perm_scores, 20, label='Permutation scores')
    ylim = plt.ylim()
    plt.plot(2 * [score], ylim, '--g', linewidth=3, label='Classification Score (pvalue %s)' %
                                                          pvalue)
    plt.plot(2 * [1. / 2], ylim, '--k', linewidth=3, label='Luck')
    plt.ylim(ylim)
    plt.legend()
    plt.xlabel('Score')
    plt.savefig("RF_permutation_plot")


def blast_parsing(result_handle):
    for blast_record in SearchIO.parse(result_handle, "blast_text"):
        print("Search %s has %i hits" % (blast_record.id, len(blast_record)))


def mean_shift_cluster(X, c_all=True, no_plot=True):  # fixme unfinished documentation
    """ Do a mean shift cluster analysis
    URL: http://scikit-learn.org/stable/auto_examples/cluster/plot_mean_shift.html
    :param no_plot:
    :param c_all:
    :param X:
    :return:
    """
    bandwidth = estimate_bandwidth(X, quantile=0.2, n_samples=500)
    ms = MeanShift(bandwidth=bandwidth, bin_seeding=True, n_jobs=8, cluster_all=c_all)
    ms.fit(X)
    n_clusters_ = len(set(map(abs, ms.labels_)))  # the abs removes the -1 if present
    if not no_plot:
        plt.figure()
        colors = 'bgcmyk' * 4  # arbitrary colors, without red, which is reserved for orphans
        for k, col in zip(range(n_clusters_), colors):
            my_members = ms.labels_ == k
            cluster_center = ms.cluster_centers_[k]
            plt.plot(X[my_members, 0], X[my_members, 1], col + '.')
            plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
                     markeredgecolor='k', markersize=10)
        if not c_all:
            my_members = ms.labels_ == -1
            plt.plot(X[my_members, 0], X[my_members, 1], "r.")
            plt.title('Estimated number of clusters: %d' % n_clusters_)
        return plt, tuple(ms.labels_)
    else:
        return list(ms.labels_)


def visualize_kpca(class_array, data_array, kpca):
    reds = class_array == 0
    blues = class_array == 1
    X_kpca = kpca.fit_transform(data_array)
    plt.figure()
    plt.plot(X_kpca[reds, 0], X_kpca[reds, 1], "ro", markersize=10)
    plt.plot(X_kpca[blues, 0], X_kpca[blues, 1], "b.")
    plt.title("Projection by KPCA")
    plt.xlabel("1st principal component in space induced by $\phi$")
    plt.ylabel("2nd component")
    return plt


def inspect_length(lengths, name="Cry_Sep"):
    plt.hist(lengths, bins=50)
    plt.title("Cry Protein Lengths")
    plt.xlabel("Protein Length (AA)")
    plt.ylabel("Frequency")
    plt.savefig(name)


def other():
    data_array, class_array = (False, False)

    # split train test
    # X = [n_samples, n_features] training samples
    # Y = [n_samples]             class labels
    X_train, X_test, y_train, y_test = train_test_split(data_array, class_array, test_size=0.4,
                                                        random_state=42)
    # or
    cv = StratifiedKFold(class_array, n_folds=10)

    # SVM things

    # Attempt at threaded nonsense, but is unnecessary
    # SvmThings.Threaded_SVM_grid_search(svmthing, data_array, class_array)

    # Regular linear kernel SVM with 60-40 train-test split
    print SvmThings.linear_svm(X_train, X_test, y_train, y_test)

    # Scaled linear kernel SVM with 60-40 train-test split
    print SvmThings.Scaled_Linear_SVM(X_train, X_test, y_train, y_test)

    # 5-fold cross-validation of a sigmoid kernel SVM
    print SvmThings.sigmoid_svm(data_array, class_array)

    # Random Forest
    do_permutated_rf(cv, data_array, class_array)

    # Reading the blast file
    with open("/home/ronald/FFpredtest/Random_BT_Plasmid_Proteins.out.NCBIblast") as blast_file:
        blast_parsing(blast_file)


def test_np_arrays():
    data_array = np.array([[1, 2, 34, 43, 43], [2, 3, 4, 12, 3], [2, 3, 41, 112, 13]])
    # noinspection PyTypeChecker
    p1 = data_array[:, 2] <= 20
    p2 = 20 >= data_array[:, 2] >= 50
    p3 = 50 >= data_array[:, 2]
    print p1  # ,p2,p3
    new_classes = []
    lists = (p1, p2, p3)
    # noinspection PyTypeChecker
    for x in p1:
        if x:
            new_classes.append(1)
        else:
            new_classes.append(0)
    print new_classes
    return lists


if __name__ == "__main__":
    print "This code is really not meant to be used for reals."


def create_csv(target_file, ran_file, bin_size):  # fixme unfinished docs
    """ Creates a comma separated value file with data generated from csv_protstats()

    :param target_file:
    :param ran_file:
    :param bin_size:
    :return:
    """
    target_out = "".join(target_file.split(".")[:-1]) + ".protstats"
    ran_out = "".join(ran_file.split(".")[:-1]) + ".protstats"
    if os.path.exists(target_out) and os.path.exists(ran_out):
        # i = raw_input("Want to overwrite the csvs?\n(Y/N):")
        i = "n"  # WIP ALWAYS SKIPS WRITING THE CSVS
        if i.upper() != "Y" or i.upper() != "YES":
            return csv_protstats({}, bin_size, True)

    with open(target_file) as a:
        di = read_infile(a, bin_size)
    with open(ran_file) as b:
        di2 = read_infile(b, bin_size)
    print "Target Dict length: " + str(len(di.keys()))
    print "Random Dict length: " + str(len(di2.keys()))
    cry_lines, header1 = csv_protstats(di, bin_size)
    ran_lines, header2 = csv_protstats(di2, bin_size)

    with open(target_out, 'w') as cry_outfile:
        cry_outfile.write(",".join(header1) + "\n")
        cry_outfile.write(cry_lines)

    with open(ran_out, 'w') as ran_outfile:
        ran_outfile.write(",".join(header2) + "\n")
        ran_outfile.write(ran_lines)

    return header1


def do_rf(X_train, X_test, y_train, y_test, header):  # fixme unfinished docs
    clf = RandomForestClassifier(warm_start=True, oob_score=True, n_estimators=1500, n_jobs=8)
    clf.fit(X_train, y_train)
    SortedPar = sorted(zip(header[1:], clf.feature_importances_), key=lambda t: t[1], reverse=True)
    print "Top 5 contributing parameters:"
    for x in SortedPar[:5]:
        print "{: <16}: {: >7.2%}".format(*x)
    score = clf.score(X_test, y_test)
    print "Accuracy score:", score
    print "OOB error:", clf.oob_score_
    pred = clf.predict(X_test)
    fpr, tpr, _ = roc_curve(y_true=y_test, y_score=pred)
    print "FPR: ", fpr[1], "\nTPR:", tpr[1]
    roc_auc = auc(fpr, tpr)
    print "AUC:", roc_auc
    print confusion_matrix(y_test, pred)
    return clf


def examine_data(header, target_stats, data_array, class_array):  # fixme unfinished docs
    # # PCA on Cry Only
    names, cry_only_data = read_single_csv(header, target_stats)
    pca = PCA(cry_only_data)
    visualize_pca(pca, header, plotname="cry_only_biplot")
    kpca = KernelPCA(kernel="rbf", fit_inverse_transform=True, gamma=0.1)
    X_kpca = kpca.fit_transform(data_array)
    target = class_array == 1
    weird = X_kpca[target, 0] >= 0.80
    print list(weird)
    print list(class_array)
    names, test_array = read_single_csv(header, target_stats)
    scatterplot_matrix(test_array[:, :8], names=names[:8]).savefig("Scatterplot")

    # Compute clustering with MeanShift
    # The following bandwidth can be automatically detected using
    names, X = read_single_csv(header, target_stats)
    labels = mean_shift_cluster(X)

    new_classes = [x + 1 for x in labels] + map(int, ("0" * (len(class_array) - len(labels))))
    # Checking stats of clustered data
    clust_dict = dict([(x, labels.count(x)) for x in set(labels)])
    print clust_dict

    print len(class_array), len(new_classes)
    print "\nnothing\n"
    print new_classes


def read_infile(infile, bin_size):
    """ Reads the fasta infile and creates dictionary, with 1 main and 8 bins of protein analysis

    :param bin_size:
    :param infile: file object
    :return: d[seq_id]= [ProteinAnalysis(seq), len(seq), ProteinAnalysis(seq_bin1), ... ]
    """
    d = {}
    c = 0  # counter for forced individuality of keys
    e = 0  # counter for error-inducing letters
    for seq_record in Bio.SeqIO.parse(infile, "fasta"):
        c += 1
        if seq_record.seq[-1] == "*":
            seq = str(seq_record.seq)[:-1]
        else:
            seq = str(seq_record.seq)
        test = [x for x in seq_record.seq[:-1] if x not in protein_letters]
        for x in test:  # testme removing non IUPAC letters too bruteforce-y
            seq = "".join(seq.split(x))
            e += 1
        n = str(seq_record.id) + "_" + str(c)  # append a random number to ensure
        d[n] = [ProteinAnalysis(seq), len(seq), ]
        for part in chunk(seq, bin_size):
            d[n].append(ProteinAnalysis(part))
    print "This many error-inducing letters removed:", e
    return d


def csv_protstats(d, bin_size):
    """ Get the data for a csv file from the BioPython Protein Analyses of full sequences and bins

    :param d: ProteinAnalysis dictionary
    :param bin_size: Amount of parts the protein is divided in
    :param nothing: Whether or not to output just the header
    :return: the_header: The header of the csv file to be written
    :return: s: the entire text file generated for a certain dictionary
    """

    s = ""

    # quit(str(len(d[d.keys()[0]])))
    for seq_id in d.keys():  # loops over seq-ids
        # fl = ""
        hy = ""
        s += seq_id
        s += "," + str(d[seq_id][1])
        s += "," + str(d[seq_id][0].molecular_weight())
        s += "," + str(d[seq_id][0].aromaticity())
        s += "," + str(d[seq_id][0].instability_index())
        s += "," + str(d[seq_id][0].isoelectric_point())
        # s += "," + str(d[seq_id][0].gravy())
        s += "," + ",".join([str(k) for k in d[seq_id][0].secondary_structure_fraction()])

        for binno in range(2, bin_size + 2):  # should loop over all the binno-ProteinAnalyses!
            # flex = d[seq_id][binno].flexibility()
            hydrophob = d[seq_id][binno].protein_scale(ProtParamData.kd, 9, 0.4)
            if len(hydrophob) < 1:  # or len(flex) < 1:
                with open("errorfile.txt", 'w') as e:
                    e.write(s + "\nhydrob_str:" + str(hydrophob) + "\nbinno:" + str(
                        binno) + "\n\n" + str(d[seq_id][binno]))
                quit(seq_id + " protein scale data is not okay")
                # print hydrophob, flex
            else:  # string output of the mean of this protein part
                hy += "," + str(sum(hydrophob) / (len(hydrophob)))
                # fl += "," + str(sum(flex) / (len(flex)))
        s += hy  # + fl
        # if s.count(',') % (9 + bin_size) != 0:  # WIP this bit is probably fine
        #     print "This should be zero:", s.count(',') % (9 + bin_size)
        #     print "This many commas should be:", 9 + bin_size
        #     print "This many commas were found:", s.count(',')
        #     with open("errorfile.txt", 'w') as e:
        #         e.write(s)
        #     quit("Amount of commas is not okay!")
        s += "\n"
    return s


def run_iupred(filepath):
    """ Runs IUPRED for prediction of disordered regions on a single fasta file

    :param filepath: full filepath to a single fasta file
    :return: d: {pos : [AA, val]}
    :return: glob_domain_spots: [(start, end), (start, end), ...]
    """
    cmd = "cd " + ff_tools_dir + "iupred/; "
    cmd += "./iupred "
    cmd += filepath + " "
    cml = cmd + "long"
    cmg = cmd + "glob "

    d = {}  # {pos : [AA, val]}
    glob_domain_spots = []
    iupred_long = run(cml, output=True)
    iupred_glob = run(cmg, output=True)
    for l in iupred_long.split("\n"):
        if not l.startswith("#") and l:
            try:
                k = l.split()
                d[k[0]] = k[1:]
            except IndexError:
                print l
                raise ("Blank lines are a nightmare!")
    for l2 in iupred_glob.split("\n"):
        if l2.startswith("#") or l2.startswith(">"):
            continue
        x = l2.split()
        if len(x) > 3 and x[-1].isdigit() and x[-3].isdigit():
            glob_domain_spots.append((x[-3], x[-1]))
        if l2.startswith("Number of"):
            domain_no = l2.split()[-1]
        assert domain_no != len(glob_domain_spots)
    return d, glob_domain_spots


def run_netsurfp():  # fixme only implement if time allows?
    pass


def run_pfilt():  # fixme only implement if time allows?
    pass


def Clustering_by_lengths(data_array):
    # < 200
    # 200 - 500
    # 500 - 850
    # > 850

    # p1 = data_array[:, 1] <= 200
    # p2 = 200 <= data_array[:, 1] <= 500
    # p3 = 500 <= data_array[:, 1] <= 850
    # p4 = data_array[:, 1] >= 850

    # targets = data_array[class_array == 1]  # data_array is just gonna be target only!!
    L_bounds = [np.percentile(data_array[:, 1], i) for i in range(0, 100, step=25)]
    new_classes = np.ndarray()

    clf = AdaBoostClassifier(n_estimators=2000)
    clf.fit(data_array, new_classes)
    clus_modfile = model_dir + "ClusteredADAboost.pkl"
    if not os.path.exists(clus_modfile):
        with open(clus_modfile, "wb") as outputfile:
            pickle.dump(clf, outputfile)

    # TESTING THE MODEL
    model_file_name = model_dir + "ClusteredADAboost.pkl"
    test_file = "/home/ronald/FFpredtest/Random_BT_Plasmid_Proteins.fasta"

    test_model(model_file_name, test_file, bin_size)


def biplot(score, coeff, par_labels=None, class_labels=None):  # fixme unfinished documentation
    """ Make a biplot of a matplotlib based PCA
    author: https://sukhbinder.wordpress.com/2016/03/02/biplot-in-python-revisited/
    :param class_labels: labels of a dataset
    :param score: the pca data object
    :param coeff:
    :param par_labels:
    :return:
    """
    xs = score[:, 0]
    ys = score[:, 1]
    n = coeff.shape[0]
    scale_x = 1.0 / (xs.max() - xs.min())
    scale_y = 1.0 / (ys.max() - ys.min())
    if class_labels:
        reds = class_labels == 0
        blues = class_labels == 1
        x = xs * scale_x
        y = ys * scale_y
        plt.scatter(x[reds], y[reds], c='r')
        plt.scatter(x[blues], y[blues], c='b')
    else:
        plt.scatter(xs * scale_x, ys * scale_y)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], color='r', alpha=0.5)
        if not par_labels:
            plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, "Var" + str(i + 1), color='g',
                     ha='center', va='center')
        else:
            plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, par_labels[i], color='g', ha='center',
                     va='center')

    plt.xlim(-1, 1)
    plt.ylim(-1, 1)
    plt.xlabel("PC{}".format(1))
    plt.ylabel("PC{}".format(2))
    return plt


def parse_fasta(lines):  # obsolete using biopython now
    """Return dictionary of {label:dna_seq}, sequences are uppercase

    :param lines: list of lines in FASTA format
    """
    seqs = {}
    label = ""
    for line in lines:
        if line.startswith('>'):
            label = line
            seqs[label] = ""
        elif line != "":
            line = line.strip()
            try:
                seqs[label] += line.upper()
            except KeyError:
                print label, "\n", line
                raw_input("Press Enter to continue...")
                continue
        else:
            continue
    return seqs


def get_cry(csvfile, enddir, results=True):
    """ Something that does what get cry is supposed to do
    will probably involve urllib
    :param csvfile:
    :param enddir:
    :param results:
    :return:
    """
    # obsolete Pipeline currently uses getCry.bash
    print csvfile, enddir, results
    pass
    #     # ebil
    #     url = "http://www.ebi.ac.uk/ena/data/view/"
    #     end = "&display=fasta"
    #
    #     acc_file =$1
    #     filedir =$2
    #     while read line; do
    #     name = `echo $line | cut -d, -f 1`
    #     acc = `echo -n $line | cut -d , -f 2`
    #     wget -O $name -q $url$acc$end
    #     echo $name,$url$acc$end, `grep '>' $name`
    #     mv. /$name $filedir$name".fasta"
    # done <$acc_file


def blast_seq(blast_type, infile, outfile, database, outfmt="", evalue=0.05):
    """ Blasts a file against a specific database

    :param outfmt: (default) Format that the blast type can take, if none, default is used.
    :param evalue: (0.05) evalue for cut off
    :param blast_type: One of: "blastp", "blastn", "blastx", "tblastn", "tblastx"
    :param infile: (type fasta) File with sequence to be blasted
    :param outfile: Blast output
    :param database: The database to blast against
    :Produces: a default blast outfile
    """
    types = ["blastp", "blastn", "blastx", "tblastn", "tblastx"]
    assert blast_type in types
    extensions = ['nhr', 'nin', 'nsq']
    tests = [database + "." + a for a in extensions]
    for x in [infile, ] + tests:
        if not os.path.exists(x):
            quit(x)

    cmd = "blastall"
    cmd += " -p " + blast_type
    cmd += " -d " + database
    cmd += " -i " + infile
    cmd += " -o " + outfile
    if outfmt:
        cmd += " -m" + outfmt
    cmd += " -e " + str(evalue)
    run(cmd)


def blast_clean(in_name, outname="", evalue=0.001, db=data_dir + "cry_blast_db/Cry_Prot_db"):
    basename = in_name.split(".")[:-1]
    cmd = "blastp "
    cmd += "-query " + in_name
    cmd += " -db " + db
    if outname:
        cmd += " -out " + outname
    else:
        cmd += " -out " + basename + ".blast"
    cmd += " -evalue " + str(evalue)
    cmd += " -outfmt '6 qseqid pident salltitles' "
    run(cmd)
    d = {}
    with open(outname) as blast_file:
        for line in blast_file:
            l = line.split()
            if l[0] not in d.keys():
                d[l[0]] = l[1:]
            else:
                d[l[0]] += l[1:]

    with open(basename + "_clean.faa", 'w') as b_clean:
        with open(in_name) as infile:
            for rec in Bio.SeqIO.parse(infile, "fasta"):
                if rec.id not in d.keys():
                    b_clean.write(rec.format("fasta"))
    print "Blasted Clean of Cry proteins!"


def double_clean(*user_args):
    """ Removes too short protein files and removes sequences with too many N's
    Author: genivaldo.gueiros@gmail.com / Ronald de Jongh 930323409080
    http://biopython.org/wiki/Sequence_Cleaner

    Input:
        [1]: your fasta file
        [2]: the user defines the minimum length. Default value 0, it means you don't have to care
        about the minimum length
        [3]: the user defines the % of N is allowed. Default value 100, all sequences with "N" will
        be in your output, set value to 0 if you want no sequences with "N" in your output
    :param user_args: List of input parameters

    """
    if len(user_args) == 1:
        n = sequence_clean(user_args[0])
    elif len(user_args) == 2:
        n = sequence_clean(user_args[0], user_args[1])
    elif len(user_args) == 3:
        n = sequence_clean(user_args[0], user_args[1], user_args[2])
    else:
        print("Thou hath issue with thine para-meters!")
        quit()

    blast_clean(n)


def scatterplot_matrix(data, names=(), **kwargs):
    """

    Plots a scatterplot matrix of subplots.  Each row of "data" is plotted
    against other rows, resulting in a n rows by n rows grid of subplots with the
    diagonal subplots labeled with "names".  Additional keyword arguments are
    passed on to matplotlib 's "plot" command. Returns the matplotlib figure
    object containing the subplot grid.


    """
    numvars, numdata = data.shape
    fig, axes = plt.subplots(nrows=numvars, ncols=numvars, figsize=(8, 8))
    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    for ax in axes.flat:
        # Hide all ticks and labels
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)

        # Set up ticks only on one side for the "edge" subplots...
        if ax.is_first_col():
            ax.yaxis.set_ticks_position('left')
        if ax.is_last_col():
            ax.yaxis.set_ticks_position('right')
        if ax.is_first_row():
            ax.xaxis.set_ticks_position('top')
        if ax.is_last_row():
            ax.xaxis.set_ticks_position('bottom')

    # Plot the data.
    for i, j in zip(*np.triu_indices_from(axes, k=1)):
        for x, y in [(i, j), (j, i)]:
            axes[x, y].plot(data[y], data[x], **kwargs)

    # Label the diagonal subplots...
    if not names:
        names = ['x' + str(i) for i in range(numvars)]

    for i, label in enumerate(names):
        axes[i, i].annotate(label, (0.5, 0.5), xycoords='axes fraction', ha='center', va='center')

    # Turn on the proper x or y axes ticks.
    for i, j in zip(range(numvars), itertools.cycle((-1, 0))):
        axes[j, i].xaxis.set_visible(True)
        axes[i, j].yaxis.set_visible(True)

    # If numvars is odd, the bottom right corner plot doesn't have the correct axes limits,
    # so we pull them from other axes
    if numvars % 2:
        xlimits = axes[0, -1].get_xlim()
        ylimits = axes[-1, 0].get_ylim()
        axes[-1, -1].set_xlim(xlimits)
        axes[-1, -1].set_ylim(ylimits)
    return fig


def plot_confusion_matrix(cm, target_names=None, title='Confusion_matrix'):
    """ Script to plot a confusion matrix all nicely

    url: http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    author: sci-kit learn developers

    :param cm: double list [[TP,b],[c,d]]
    """
    fig = plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title(title)
    plt.colorbar()
    if target_names:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(title)
    plt.close(fig)


def get_meta(indir):
    """ Gets the line lenghts and filepaths of all downloaded cry fasta files in a .meta file

    :param indir: directory where the .fasta files are.
    :return: meta_filename: name of the file it produced.
    """
    meta_filename = data_dir + "known_cry_before.meta"
    if os.path.exists(meta_filename):
        meta_filename = data_dir + "known_cry_after.meta"
    cmd = "wc -l " + indir + "*.fasta | sort -g > " + meta_filename
    if os.system(cmd) != 0:
        print cmd
        print "has failed!"
    return meta_filename

# # cool snippets:
